# 2. Conception assistée par ordinateur

Dans cette partie, je vais décrire comment j'ai modélisé le première objet que j'ai imprimé en 3D. Dans un premier temps, je décrirai la recherche derrière cette objet puis je decrirai les phases de modélisation.

## Recherche

Pour cette exercice, il était demandé de reproduire l'objet choisi lors de notre visite au musée du design ou de réalisé un objet plug-in à partir de celui-ci. La modélisation de mon objet ne représentant pas grand intérêt, j'ai décidé de m'orienter sur un objet plug-in (ci-dessous l'objet de base, pour plus d'informations sur le choix de l'objet, voir final project).

![](../images/Tv.jpg)
RICHARD SAPPER, MARCO ZANUSO
Black 201
Télévision
1969, PMMA, ABS, electric circuit, Brionvega, ITA

Afin de décider quel objet il était possible de réaliser et sous quel forme, j'ai commencé par réaliser des recherches sur papiers et notament à définir les différents concepts de son design (voir ci dessous les différents concepts / caractéristiques émergeants de l'objet).

![](../images/concept.png)
Concepts

A partir de ces concepts, j'ai décidé de concevoir une télécommande qui aurait pu exister avec cette télévision. J'ai voulu faire une télécommande simple et qui reprenait différants concepts que j'avais déterminé. Cette télécommande est donc un parallélépipède rectangle pour le côté box dans lequel une fente aux angles arrondies permet de dissimuler les touches.

![](../images/design.png)
Croquis télécommande

A cette phase, aucune proportion n'est défini mais l'idée générale de l'objet est fixée, je décide donc de passer à la phase de modélisation de l'objet sur Fusion.


## Modélisation fusion

* **Qu'est ce que fusion**

Fusion est un logiciel de modélisation 3D Autodesk permettant de modéliser des volumes à partir "d'esquisse" réalisé en 2 dimensions dans un plan fixé.

* **Les bases**

La première étape pour créer un volume dans fusion est de créer une esquisse que l'on peut créer en cliquant sur l'icone "créer une esquisse" (voir ci dessous)

![](../images/fusion/1.png)

Une fois l'esquisse créée, il est nécessaire de sélectionner un plan dans lesquel dessiner cette esquisse. (voir ci dessous)

![](../images/fusion/2.png)

Dans le mode esquisse, il faut dessiner une forme de base sur laquelle on viendra par la suite appliquer des fonctions pour en faire un objet 3D. Sur l'image ci dessous, on peut voir en haut à gauche les outils de dessins utilisé pour faire l'esquisse. Aussi en bas à droite de cette image, on peut voir qu'une "action" a été ajoutée à la timeline, cette timeline regroupe l'ensemble des actions effectuées sur l'objet, il est donc possible à n'importe quel stade du processus de revenir en arrière ou de modifier un de ces étapes.

![](../images/fusion/3.png)

Lorsque l'on commence à tracer une esquisse, il est toujours important de se fixer à l'origine au début, afin d'ancrer l'esquisse dans l'espace. Pour ce faire, il est nécessaire d'avoir un moins un sommet sur l'origine (voir ci-dessous), si un segment ou une forme n'est pas fixé, elle apparaitra alors en bleu au lieu de noir (voir ci-dessous)

![](../images/fusion/4.png)

Si une partie du dessin n'est pas fixé, on peut le fixer dans l'espace avec des cotations vis-à-vis d'un objet déjà fixé à l'origine (voir ci-dessous).

![](../images/fusion/5.png)

Il est aussi possible d'appliquer des contraintes aux objets pour les forcers dans une situation. Il existe plusieurs types de contraintes on les retrouve à côté des outils dessins (voir image ci-dessous). Ci-dessous on peut voir un exemple de traits contraints perpendiculairement (indiqué par le un logo à l'emplacement de la contrainte).

![](../images/fusion/6.png)

Une fois l'esquisse terminée et fixée, on peut terminer l'esquisse et ainsi passer dans la phase 3D. Pour créer un volume à partir d'une esquisse, il existe différents outils que l'on peut voir dans l'image ci-dessous.

![](../images/fusion/7.png)

Pour illustrer mon exemple, j'ai réalisé une extrusion sur cette esquisse pour en faire un parallélépipède rectangle. Il est possible de choisir différents paramètres sur ces outils, la face sur laquelle on l'applique, le sens, la valeur appliquée et d'autres paramètres selon les outils.

![](../images/fusion/8.png)
![](../images/fusion/9.png)

Une fois le volume créé, on peut y appliquer différents types de modifications comme indiqué dans l'image ci dessous.

![](../images/fusion/10.png)


* **Ma modélisation**

L'objet étant assez simple dans sa forme, la phase de modélisation ne fut pas des plus compliqué, cependant c'est durant cette phase que j'ai décidé des proportions de l'objet, j'ai donc dans un premier temps créer une esquisse de la base du parallélépipède rectangle qui déterminerait les dimensions de l'objet et que j'ai ensuite extrudé.

![](../images/2/1.png)

Ensuite, j'ai réalisé une esquisse du décroché que j'ai extrudé afin de délimiter la zone dédiée aux touches.

![](../images/2/2.png)

![](../images/2/3.png)

Enfin, j'ai réalisé une esquisse permettant d'extruder par la suite les différentes touches. Durant cette étapes, j'ai décidé de faire une télécommande minimaliste avec le moins de touches possibles afin de coller à l'esthètique et l'époque de la télévision.

![](../images/2/4.png)

![](../images/2/5.png)

Une fois le modéle terminé, je suis passé à la phase d'impression.
