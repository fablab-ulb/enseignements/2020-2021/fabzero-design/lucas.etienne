# 3. Impression 3D

Dans ce module, réalisé à la suite du module 2, j'ai fait ma première impression 3D sur les imprimantes du fablab.

## Configuration de l'impression

* **Exporter le modèle**

Pour exporter le modèle depuis fusion, il est important d'exporter le modéle dans un format lisible par le logiciel permettant de configurer les paramètres d'impressions. Le format désiré est un format .stl, pour exporter l'objet dans ce format depuis fusion, suivre les étapes ci-dessous.

![](../images/3/1.png)

![](../images/3/0.png)

* **Installer PrusaSlicer**

Afin de créer un fichier lisible par l'imprimante et configurer de la manière que l'on veut, il est nécessaire de télécharger le logiciel associé à l'imprimante 3D que l'on veut utiliser (cf PrusaSlicer), dans notre cas PrusaSlicer pour la prusa I3 MK3S. Une fois le logiciel installé, on peut importer le modèle dedans.

![](../images/3/3.png)

* **Importer le modèle**

Une fois dans PrusaSlicer, suivre les indications ci-dessous pour importer l'objet sur le plateau.

![](../images/3/4.png)

* **Paramètrer l'impression**

Afin de paramètrer l'impression, il est important de respecter quelques points clés. Dans un premier temps, passer le logiciel en mode expert dans les réglages d'impression, celà permettra d'avoir plus de contrôle sur les paramètres d'impression (voir image ci-dessous pour basculer le logiciel).

![](../images/3/6.png)

Une fois en mode expert, il est nécessaire de commencer à configurer les paramètres d'impressions.

Dans un premier temps, penser à configurer l'impression selon les caractéristiques de l'imprimante 3D et du filament utilisé pour cela, aller sur le panneau de contrôle comme sur l'image ci dessous et changer les paramètres si besoin.

![](../images/3/15.png)

Basculer ensuite dans les paramètres d'impressions, les premiers paramètres à changer sont la hauteur de couche et la hauteur de couche minimal qui doivent tous le temps être à 0,2 mm.

Ensuite il est important de régler les périmètres des parois verticals à un minimum de 3 pour avoir une certaine rigidité de la pièce, elle peut être augmentée pour plus de résistance mais il est préférable de modifier le remplissage pour augmenter la resistance d'un objet.

Selon l'objet, cette phase n'est pas forcément nécessaire mais vu la nature de mon objet, il est nécessaire que les coques horizontales est le même aspect de chaque côté. Pour ce faire aligner les paramètres des coques horizontales sur les mêmes valeurs pour le haut et le bas de l'objet comme sur l'image ci-dessous. Il peut être nécessaire d'ajuster les paramètres de ces coques selon l'effet escompté.

![](../images/3/8.png)

Régler le remplissage est une phase importante pour déterminer la solidité d'un objet, le remplissage idéal pour un objet se situe entre 10 et 15 %. On peut aller jusqu'à 35% pour une rigidité importante de l'objet, au delà ça ne sert à rien car ce n'est que du gaspillage.

Il y a plusieurs choix sur les types de remplissages, les plus solides sont les giroïdes et les nids d'abeilles. Dans mons cas, je suis parti sur une grille avec 10% de remplissage afin de réduire le temps d'impression.

![](../images/3/10.png)

Le paramétre suivant (jupe et bordure) permet de créer physiquement sur le plateau un contour autour de l'objet afin de vérifier avant le début de l'impression des dimensions de l'objet par rapport au plateau et d'éviter de gaspiller des heures d'impression et de la matière si l'objet est plus grand que le plateau. Plusieurs paramétres peuvent être modifié, les nombres de boucles réalisézq, la distance par rapport à l'objet et la hauteur de la jupe sont les plus courament modifié. Une hauteur de jupe d'une couche est suffisante pour vérifier les dimensions de l'objet.

![](../images/3/11.png)

Enfin, paramétrer les supports est une phase importante selon l'objet que l'on veut imprimer, dans mon cas aucun n'étaient nécessaires mais si l'objet en nécesite il suffit de cocher la case générer les supports. Il est préférable au maximum de diposé l'objet sur le plateau de manière à avoir le moins de supports possible dans un soucis d'économie de matière et de finition de l'objet.

![](../images/3/12.png)

* **Vérifier le temps d'impression**

Une fois paramétré pour obtenir le temps d'impression, retourner sur l'onglet plateau et appuyer sur découper maintenant.

![](../images/3/17.png)

On obtient alors différentes informations, notamment le temps d'impression ainsi que les différents types de fonctionnalités selon les parties de l'objet à imprimer.

![](../images/3/16.png)

Dans mon cas je me suis rendue compte que le temps d'impression était beaucoup trop important pour l'objet que je voulais réaliser (3 h 5 min), même en jouant sur les paramètres pour réduire le temps d'impression. J'ai donc décidé de réduire la dimension de mon objet par 2 afin d'obtenir un temps d'impression plus correct en utilisant le facteur de redimensionnement.

![](../images/3/18.png)

Une fois la dimension de l'objet diminué par 2 j'ai pu obtenir un temps d'impression plus correct (42 min) et j'étais prêt pour l'impression.

![](../images/3/20.png)
* **Exporter le G-code**

Exporter le G-code comme sur l'image ci-dessous permet de créer un document lisible par l'imprimante 3D.

![](../images/3/21.png)

* **Lancer l'impression**

Déposer le fichier sur une carte SD puis l'insérer dans le port de l'imprimante, sélectionner le bon fichier puis lancer l'impression.

## Résultat

La première impression que j'ai eu en tenant l'objet dans ma main, c'est la rapport à l'échelle qui m'est apparu comme approprié / légérement petit alors que j'avais réduit de 50 % les dimensions de l'objet. Celà m'as fais réalisé comment la modélisation numérique peut induire en erreur sur les proportion d'un objet.

La deuxième chose que j'ai pu remarqué est la différence de finition selon les faces qui ne sont pas égales.

![](../images/2/8.png)

![](../images/2/9.png)

![](../images/2/10.png)

Cette exercice m'a permis de découvrir certains aspects techniques de l'impression 3D mais aussi d'approfondir ma connaissance de l'objet. En effet l'analyse réalisé pour concevoir cette télécommande m'a permis de découvrir certains aspects de l'objets qui ne m'éttaient pas apparus clairement lors de mon choix de celui-ci. Je peux notament citer l'écran incurvé ou les touches de couleurs sur le dessus du téléviseur.

## Useful links

- [PrusaSlicer](https://www.prusa3d.fr/drivers/)
