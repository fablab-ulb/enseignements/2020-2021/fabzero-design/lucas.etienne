# 4. Découpe assistée par ordinateur

Cette semaine j'ai réalisé une lampe à l'aide d'une découpeuse laser.

## Recherche

Dans un premier temps, j'ai réalisé des dessins de principes pour m'aider à imaginer l'objet sur papier (voir ci-dessous).

![](../images/new/11.png)

Mon objet se base sur un principe de pliage en accordéon me permettant de faire un objet en 3D à partir d'une seule découpe.
Je suis rapidement passé à la phase de modélisation / dessin sur ordinateur afin de déterminer les dimensions de l'objet et avoir un premier rendu.

## Modélisation sketchup

Afin d'avoir un aperçu rapidement de l'objet j'ai décidé de le modéliser sur sketchup car j'ai actuellement plus de facilités avec ce logiciel. Ci dessous sont les diffèrentes étapes de modélisations.

![](../images/new/10.png)

![](../images/new/2.png)

![](../images/new/3.png)

A la suite de ça, j'ai décidé que l'objet me convenait et qu'il était temps de faire un document en 2D qui me permettrait ensuite de découper mon objet dans une plaque.

## Dessin autocad

A cette phase j'ai simplement dessiné le motif qui serait répété pour former mon objet. A cette phase j'ai aussi associé des couleurs aux traits en fonction de ce que je voulais que la découpeuse laser fasse.

Motif de base et dimensions

![](../images/new/4.png)

Motif de bases + espacements des plaques

![](../images/new/5.png)

Accordéon assemblé

![](../images/new/6.png)

Gravures faibles

![](../images/new/7.png)

On peut voir sur ces différentes images le processus de dessins que j'ai réalisé. On peut voir en rouge les éléments découpé, en bleu les éléments gravés qui seront pliés, en vert des gravures faibles pour les motifs et en jaune des gravures trés faibles pour les motifs.

## Découpe

Pour réaliser la découpe il m'a été nécessaire de créer un fichier .svg, j'ai donc produit un pdf depuis Autocad que j'ai ensuite ouvert avec Illustrator et ré-enregistrer en .svg.

![](../images/new/9.png)

Il s'avère que cette technique ne fonctionne pas pour réaliser des .svg qui fonctionne avec Driveboardapp, le logiciel pilotant le laser. J'ai donc ouvert le fichier sur inkscape afin de ré-enregistrer le document dans un format .svg fonctionnel avec driveboardapp.

Une fois cette étape réalisée, j'ai ouvert le document dans driveboardapp (voir image ci-dessous).

![](../images/new/12.png)

J'ai par la suite réglé les paramètres découpes que l'on peut voir ci-dessous. Le paramètre F correspond à la vitesse de la machine et le pourcentage à la puissance de celle-ci. Il est préférable de ne pas dépasser les 80 % pour la découpe. Attention, il est important de bien organiser les couleurs afin de réaliser les gravures avant les découpes pour éviter que la matière ne bouge pendant la découpe.

![](../images/new/13.png)

### Résultats

Une fois la découpe lancée, le laser n'a pas pris plus de 10 min à faire la découpe.

![](../images/new/16.png)

Il s'est avéré lors de la récupération de la pièce que la vitesse de découpe était en fait trop rapide et que le laser n'était pas passé aux travers du matériaux à certains endroits. J'ai donc du finir les découpes à la main.

![](../images/new/14.png)

![](../images/new/15.png)

En réalisant les pliages il s'avère qu'il serait nécessaire d'ajouter des piéces pour maintenir l'accordéon afin qu'il ne se détende pas avec le temps.
