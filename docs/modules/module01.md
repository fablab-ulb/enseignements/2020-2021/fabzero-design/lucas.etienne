# 1. Etat de l'art et documentation

Cette semaine, j'ai réalisé mes premiers pas sur la plateforme du GitLab. J'ai réalisé l'installation de plusieurs logiciels me permettant de rédiger cette documentation ainsi que ma presentation sur la page d'accueil.

## Etapes

* #### Installation de bash

Dans un premier temps, j'ai réalisé l'installation de bash car mon ordinateur fonctionne sous Windows et j'avais besoin d'accéder à un terminal de commande fonctionnelle. Pour installer bash, j'ai suivi un tutoriel (cf Tutoriel bash). J'ai rencontré une première difficulté pour exécuter bash depuis le terminal de commande windows (cf image 1).

![Image 1](../images/bash.png)

Pour résoudre ce problème, il était nécessaire d'installer Ubuntu depuis le Microsoft store (chose non précisé dans le tutoriel, cf link ubuntu). Après l'installation d'Ubuntu, tout était en ordre et j'ai pu suivre le tutoriel à la lettre.

* #### Installer Git



J'ai réalisé l'installation de Git afin d'accéder à mes données en local. Pour l'installation de Git j'ai suivi un tutoriel du fabzero (cf Tutoriel git), ci dessous les étapes que j'ai suivi pour l'installation du git et sa configuration.

* **Etape 1**

Installer le git et le configurer.

![Image 1](../images/1.png)

* **Etape 2**

Créer une clé SSH.

Il m'a été nécessaire de créer une clé SSH afin de sécuriser ma connexion au gitlab. Pour ce faire, j'ai suivi un tutoriel dont on peut voir les étapes ci-dessous. (cf tutoriel Clé SSH)

![Image 1](../images/11.png)

* **Etape 3**

Se connecter au git lab avec la clé SSH.

Pour cette étape une, j'ai rencontré une légère difficulté afin de trouver ou était le fichier contenant la clé et quelle clé était la bonne. Le document se trouvait en fait dans un fichier à la racine de mon ordinateur comme dans l'image ci dessous. Enfin, j'ai réalisé en essayant les 2 clés sur le git lab que la clé a utilisé était celle en .pub.

![Image 1](../images/12.png)

Mis à part ça, la création de la clé et son dépôt sur gitlab (Allé dans settings puis SSH Keys et enfin ouvrir avec un document de traitement de texte la clé, la copier et la coller dans l'encadré Key) se sont passé sans problème.

* **Etape 4**

Cloner ses documents.

Enfin, j'ai pu cloner mes documents du gitlab sur mon ordinateur en utilisant la commande ci dessous (cf tutoriel Git), il est nécessaire de recupérer l'adresse fourni sur le gitlab pour s'identifier voir ci-dessous.

![Image 1](../images/13.png)

![Image 1](../images/14.png)

* #### Apprentissage du Markdown

Le markdown est un language informatique, il m'est utile actuellement pour écrire cette documentation, pour plus d'information lire l'introduction du tutoriel fabzero pour apprendre le Markdown (cf Tutoriel Markdown). Après avoir réaliser ce tutoriel j'étais donc prêt à réaliser ma première documentation.

* #### Rédaction de l'index et du module 1.

L'index contient ma presentation personnel ainsi que celle de l'objet que j'ai choisi lors de notre visite au musée du design de Bruxelles. Le module 1 est celui que vous êtes en train de lire.

A cette étape, légère difficulté sur la mise en page du site web, la présentation est correcte sur le preview du Gitlab ainsi que sur le preview intégré à Atom mais je n'ai pas bonne sur le site (cf Image 1,2 & 3).

Résolution: ce problème était enfaite créer par une ligne de code HTML insérer pour justifier le texte. J'ai donc supprimer cette commande pour résoudre le problème. 


## Useful links

- [Tutoriel bash](https://korben.info/installer-shell-bash-linux-windows-10.html)
- [Link ubuntu](https://aka.ms/wslstore)
- [Tutoriel Git](https://github.com/Academany/fabzero/blob/master/program/basic/git.md)
- [Tutoriel Clé SSH](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
- [Tutoriel Markdown](https://www.markdowntutorial.com/)

## Gallery

![Image 2](../images/4.png)
Image 1

![Image 3](../images/2.png)
Image 2

![Image 4](../images/3.png)
Image 3
