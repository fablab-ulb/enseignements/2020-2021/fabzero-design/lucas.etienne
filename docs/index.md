### Lucas Etienne
![](images/avatar.jpg)

Bonjour !

Je m'appelle Lucas et j'ai 22 ans. Je suis actuellement en première année de Master d'architecture à l'école d'architecture La Cambre Horta à Bruxelles. Sur ce site, vous pourrez explorer mes recherches et mon parcours réalisé dans le cadre du cours d'un cours de design dispensé au Fablab de l'ULB.

### Mon histoire

Originaire du Sud-Ouest de la France, j'ai grandi dans une petite ville renommée pour son vin, Gaillac. A l'obtention de mon bac je me dirige vers un diplôme universitaire de technologie en génie civil. J'ai réalisé ces études en 2 ans à l'université de Toulouse Paul Sabatier. Suite à l'obtention de mon diplôme, j'ai intégré l'école nationale supérieure d'architecture de Lyon en deuxième année. J'ai obtenu ma licence à Lyon et j'ai donc décidé de réaliser un Erasmus d'une durée d'un an à Bruxelles.

### Mes passions

* wakeboard
* nouvelles technologies

### Mes objectifs

* Développer mes compétences de modélisations 3D
* Apprendre à utiliser les différentes machines du Fablab
* Produire au moins 1 objet et plusieurs prototypes
* Développer des compétences de design

### Mon objet

Lors d'une visite du Design Museum Brussels, nous devions séléctionner un objet de la collection plastique qui nous interroger ou nous interpeller. Cette objet nous servira de base pour réaliser un détournement de celui lors du cours de design.

![](images/Tv2.png)
![](images/Tv1.png)

RICHARD SAPPER, MARCO ZANUSO
Black 201
Télévision
1969, PMMA, ABS, electric circuit, Brionvega, ITA

Cette télévision produite par Brionvega fut conçu en 1969 par 2 designers italien et allemand, respectivement Marco Zanuso et Ricgard Sapper. C’est le premier petit poste de télévision conçu comme une pièce décorative et reçoit le prix Compaso d’Oro en 1970 ainsi qu’une mention honorable à la biennale de deign de Ljubljana en 1973. Cette télévision émerge d’une recherche d’un nouvel esthétisme des produits éléctroniques, le principe étant de masquer la vraie fonction de l’objet lorsque la télévision est éteinte. Elle fut produite de 1966 à 1974.

J’ai choisi cet objet parmis tous les autres parce qu’il m’a intrigué à première vue sur sa fonction. Dans un deuxième temps, c’est la forme très simple et angulaire que j’ai trouvé intéressante qui m’a rappelé l’ordinateur NeXTcube.
Aussi, c’est la continuité de la matière qui m’a plu dans cet objet, le fait que l’écran ne soit pas en décalage avec le corps de la télévision mais dissimulé dans celui-ci, donnant cette sensation d’uniformité du volume. Enfin, la finition de l’objet en acrylique permet de refléter les environs de l’objet ce qui ajoute encore à la sensation d’objet «mystère».
