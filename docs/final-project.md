# PROJET FINAL

## FRAME

Mon objet est une lampe qui s'inspire de la Black 201 Television Set. Mon attitude d’approche peut être décrite par l’inspiration et l’influence car j'ai décidé de reprendre certains concepts clés de la télévision et d'en faire la base de mon objet. Le but est de réaliser une lampe qui à la façon de la télévision ne laisse pas transparaître sa fonction d'origine lorsqu'elle est éteinte. Pour ce faire j'ai décidé de réaliser un cadre contenant un dispositif produisant de la lumière et la projetant sur une surface. Ainsi ce cadre par son aspect sculptural permet de ne dévoiler sa fonction que lorsqu’on l’allume. Enfin la création de cette lampe m’a permis de développer un système modulable que l’on peut décliner selon ses besoins (catalogue présenté plus loin dans la présentation).

![](images/5/47.png)

- [Fichier svg pour la découpe](images/6/cadre.svg)

## CATALOGUE

Dans cette partie je vais présenter plusieurs idées de lampes possibles avec le système développé et à la suite je donnerai les informations nécessaires à prendre en compte pour réaliser une lampe à partir de ce système.

#### Sol

![](images/5/48.png)

#### Mur

![](images/5/49.png)

![](images/5/50.png)

#### Plafond

![](images/5/53.png)

![](images/5/51.png)

![](images/5/52.png)

![](images/5/54.png)

Pour réaliser une lampe à partir de cette technique il faut savoir dans un premier temps que l’on est limité par la dimension du panneau que l'on utilise, à savoir 122 cm par 244 cm. Une fois ce paramètre pris en compte il est nécessaire de connaître les dimensions de la pièce à éclairer ainsi que l'intensité lumineuse voulue. Pour cela on peut se référer à ce type de tableau:

 ![](images/5/55.png)

 ![](images/5/56.png)

Une fois cette valeur prise en compte il suffit de réaliser ce calcul pour connaître sa longueur de ruban nécessaire :

[1,3 (facteur éclairage indirect) x Eclairement moyen (lux) x surface de la pièce (m²)] / Intensité lumineuse du ruban (lumen / m) = longueur de ruban (m)

Une fois cette valeur connue il suffit de couper le fil LED aux dimensions désirées et de l'assembler pour connaître les dimensions précises du cadre.

Attention il est nécessaire de se renseigner sur la puissance de l'adaptateur 12V nécessaire pour alimenter la totalité du ruban (cf. spécification technique du ruban).  

Une fois la forme et les dimensions choisies il est nécessaire de réaliser un fichier vectoriel SVG de celui-ci. Je recommande sur les dimensions du cadre d'utiliser 4 cm de largeur sur celui-ci, de manière à pouvoir creuser avec une défonceuse numérique ou une machine CNC l'espace pour intégrer les LED. Aussi il est préférable de prévoir au moins 2 cm de largeur sur la cadre pour intégrer les LED.


## REFERENCE

### Référence: Black 201 Television Set

Lors d'une visite du Design Museum Brussels nous devions sélectionner un objet de la collection plastique qui nous interrogeait ou nous interpellait. Cet objet m’a servi de base de réflexion lors du cours de design.

![](images/Tv.jpg)
RICHARD SAPPER, MARCO ZANUSO
Black 201
Télévision
1969, PMMA, ABS, electric circuit, Brionvega, ITA

Cette télévision produite par Brionvega fut conçue en 1969 par 2 designers, un italien et un allemand, qui sont respectivement Marco Zanuso et Ricgard Sapper. C’est le premier petit poste de télévision conçu comme une pièce décorative. Il reçoit le prix Compaso d’Oro en 1970 ainsi qu’une mention honorable à la biennale de design de Ljubljana en 1973. Cette télévision émerge d’une recherche d’un nouvel esthétisme des produits électroniques, le principe étant de masquer la vraie fonction de l’objet lorsque la télévision est éteinte. Elle fut produite de 1966 à 1974.

J’ai choisi cet objet parmi tous les autres parce qu’il m’a intrigué à première vue sur sa fonction. Dans un deuxième temps, c’est la forme très simple et angulaire que j’ai trouvé́ intéressante qui m’a rappelé́ l’ordinateur NeXTcube.
Aussi, c’est la continuité́ de la matière qui m’a plu dans cet objet, le fait que l’écran ne soit pas en décalage avec le corps de la télévision mais dissimulé dans celui-ci, donnant cette sensation d’uniformité́ du volume. Enfin, la finition de l’objet en acrylique permet de refléter les environs de l’objet ce qui ajoute encore à la sensation d’objet « mystère ».

### Attitude de conception

#### Définitions

**Influence :**

* Action d’une personne, d’une circonstance ou d’une chose qui influe sur une autre.
* VIEUX Fluide provenant des astres et agissant sur la **destinée** humaine.
* **Action exercée sur** (qqn ou qqch.). effet.
* (PERSONNES) Action volontaire ou non (sur qqn). ascendant, empire, emprise, pouvoir.
* Action morale, intellectuelle
* **Ascendant** de quelqu'un sur quelqu'un d'autre
* **Pouvoir** social et politique de quelqu'un, d'un groupe, qui leur **permet d'agir sur le cours des événements, des décisions prises,** etc.

**Inspiration :**

* (Par analogie) Sorte de **souffle divin** qui pousse à tel ou tel acte.
* (Par extension) **Acte de stimulation** de l’intellect, des émotions et de la créativité **à partir d’une influence.**
* Souffle créateur qui anime les artistes, les chercheurs. L'inspiration poétique. Attendre l'inspiration.
* Action d'inspirer, de conseiller qqch. à qqn ; résultat de cette action. influence, ➙ instigation.
* Mouvement intérieur, **impulsion qui porte à faire, à suggérer ou à conseiller quelque action:** Suivre son inspiration.
* Particulièrement. L'enthousiasme qui entraîne les poëtes, les musiciens, les peintres

____

Les 2 mots qui m'ont le plus parlé sont **influence** et **inspiration**. Avec pour base les définitions sélectionnées, j'ai choisi certains termes parmi celles-ci. Ces termes sont ceux qui résonnent le plus avec la vision que j'ai de l'exercice. J'ai ensuite associé ces termes d'une part à ma référence, la télévision et l'objet que je vais créer.

Référence = ascendant / souffle divin / acte de stimulation / Pouvoir

Réaction par rapport à ma référence = Destinée / Action exercée sur / permet d'agir sur le cours des événements, des décisions prises / impulsion qui porte à faire, à suggérer ou à conseiller quelque action.

Dans cette classification on retrouve d'une part ce que la référence apporte dans ma réflexion, l'idée d'un élément de référence dictant les règles et principes de conception de mon futur objet. D'une autre part la réponse de ma réflexion vis-à-vis de cette référence, une réinterprétation de ces règles et principes de conception.

L'approche que je souhaite employer pour cet exercice consiste à reprendre des éléments clés du design de la Tv Black 201, pour les utiliser dans un futur design d'objet. C'est un processus que j'ai déjà utilisé lors des précédents exercices. Ces éléments clés peuvent être caractérisés de stimulus dans mon processus de conception, c'est en ce sens que les termes de pouvoir, souffle divin et ascendant sont appropriés pour caractériser ma référence. De la même façon mon objet sera la destination que je souhaite atteindre.

La méthode de travail que je souhaite mettre en place se basera sur une liste des grands principes constituants l'objet de référence et comme résultat un objet qui fera écho à cette liste. Ci-dessous la liste des caractéristiques de la tv Black 201.

![](images/5.png)

## Construction de la lampe

Pour réaliser cette lampe il m’a tout d’abord été nécessaire d’obtenir les matériaux de fabrication. Dans un premier temps je me suis procuré du MDF noir pour réaliser le cadre. J’ai choisi ce matériau pour plusieurs raisons, la première est le fait que ce type de bois est teinté dans la masse et je n’avais donc pas besoin de peindre le cadre. Il était important pour moi de ne pas avoir à peindre le cadre pour garder une finition propre de l'objet. En effet le cadre étant présenté comme un objet sculptural il était primordial que les finitions de celui-ci soit irréprochables.

![](images/5/43.png)

Pour l'éclairage je me suis orienté sur des rubans LED qui sont suffisamment fins et qui permettent une puissance d'éclairage adéquate pour mon projet. J'ai opté pour un ruban LED IP 20 car la résistance à l'eau de celui n'étais pas nécessaire et cela permet de réduire les dimensions de celui-ci.

![](images/5/33.png)

Dimensions du ruban en centimètres : 500 x 1 x 0.2 cm

Pour cette lampe il était nécessaire que le ruban LED suive le cadre. J'ai donc acheté des connecteurs permettant de réaliser des raccords à 90° en coupant les rubans LED. Les rubans étant coupable l'opération consiste à couper le ruban à certaines longueur en accord avec les dimensions du cadre et de les raccorder grâce au module ci-dessous.

![](images/5/34.png)

Enfin pour la puissance j'ai opté pour un ruban LED 12V avec des LED de 5mm par 5mm et une quantité de 60LED/m. Ce type de bandeau fournit 900 lumen/m. Ci-dessous les caractéristique du ruban.

![](images/5/35.png)

Il m'a aussi été nécessaire de me fournir un adaptateur 220V - 12 V 60W pour permettre le fonctionnement du ruban LED ainsi que des connecteurs en plus en cas de problèmes.

![](images/5/41.png)

Une fois le matériel acheté j'ai pu commencer à réfléchir aux dimensions que je souhaitais pour la lampe vis à vis de son éclairage et de son utilisation.

Lors de l'achat de l'adaptateur j'ai décidé de prendre un adaptateur fournissant 60W de puissance. Avec ces 60W j'ai eu la possibilité d'alimenter 410 cm de ruban LED (60W / 14,4 W/m = 410 cm approximativement)

Avec 410 cm de ruban cette lampe fournit (4,1m x 900lm/m = 3690 lumens) 3690 lumens.

En sachant que mon éclairage est indirect on peut considérer une perte de 35-40 % de l'éclairement vis à vis de la surface de réflexion.

On peut donc considérer que le lampe produira (3690 x 0,4 = 1500) 1500 lumens en sachant que l'éclairage pour un salon doit être autour des 150-200 lux (lumen / m²) on peut espérer éclairer (1500 lumens / 150 lux) 10 m² avec la configuration de cette lampe.

Avec ça en tête, j'ai réalisé un cadre ayant des proportions appropriées pour être posé au sol.

![](images/5/44.png)

Il a été primordial de déterminer correctement ces dimensions afin de pouvoir insérer le ruban LED dans l'encoche prévu à cette effet, pour ce faire j'ai réalisé une encoche de 2cm pour avoir de la marge dans le placement du ruban. Pour déterminer les longueurs des rubans il faut savoir qu'ils ne sont découpables que tous les 5 cm et aussi que le connecteur d'angle représente 1,2 cm en bout. Avec ça en tête j'avais les dimensions du cadre.

Pour la construction du cadre je me suis servi de la Shaper Origin avec laquelle j'ai réalisé la découpe et la fente.

![](images/5/45.png)

Une fois découpé et peint j'ai pu découper aux dimensions prévues et assembler le ruban LED avant de l'installer dans le cadre.

![](images/5/46.png)

## Recherche

La première phase de recherche réalisée sur papier consistait plus sur un travail de forme et de concept car je n'étais pas encore fixé sur ce que je voulais réaliser et comment le faire. Le première objet auquel j'ai pensé est un cendrier, on peut voir les différents essais ci-dessous du moins abouti au plus abouti.

![](images/5/5.png)
![](images/5/2.png)

Je suis rapidement passer à autre chose car les possibilités était trop limités pour cet objet et la faisabilité compliquée avec les outils de production du fablab compte-tenu à la chaleur.
Cette recherche m'a cependant permis d’obtenir une base de réflexion pour la suite de mon travail. En effet, les traits caractéristiques de l'objet que j'ai emprunté à ma référence sont ceux qui par la suite seront ceux qui guideront ma réflexion. L'objectif de ce cendrier était d'en cacher l'utilité lorsqu'il n'est pas utilisé de la même façon que la télévision. Cet aspect d'objet "mystère" sera le fil conducteur pour le reste de ma réflexion. Ensuite, ce sont plus au niveau des questions morphologiques qu'il me servira de référence, l'aspect cubique, le principe de grille emprunté de la télévision.

Dans un second temps, j'ai commencé à m'orienter sur un objet plus pratique et réalisable au sein du fablab. J'ai donc pensé à une lampe reprenant ces concepts. On peut observer ci-dessous des premiers croquis test.


![](images/5/3.png)
![](images/5/4.png)


Le principe de cette lampe serait de créer une armature à l'aide de l'imprimante 3D. Cette armature en cube percé permettrait de venir glisser des feuilles de polypropylène dans des encoches prévues à cette effet sur le cadre. Ainsi, cette objet feuille de polypropylène viendrait faire écran et diffuser la lumière. Aussi, il serait possible d'intégrer des filtres à l'intérieur de la lampe pour modifier les ambiances ou la luminosité produite par l’objet.

Par la suite, j'ai essayé de me détacher de la lampe avec une seule source de lumière et de partir sur des sources de lumière plus linéaire et j'en suis arrivé aux 2 essais ci-dessous qui, je trouve, correspondait mieux à l'idée que je me faisais d'un objet avec sa fonction cachée. Le but est ici de créer une lampe, qui lorsque elle est éteinte, agit comme une "sculpture" et ne laisse pas transparaitre sa fonction première. Dans un premier temps, j'ai travaillé sur l'idée du cube complétement ouvert que l'on peut voir en premier. L'idée me plaisait beaucoup, cependant techniquement je ne voyais pas comment réaliser un assemblage entre le cadre en plastique contenant les LED et le polypropylène les cachant. J'ai donc poursuivi ma réflexion sur un second objet que l'on peut voir ci-dessous.

Cette lampe se constitue d'un assemblage de cadres imprimés en 3D liés par des bandes de polypropylène, emboités de part et d'autres dans les cadres comme indiqué dans la coupe ci-dessous.

![](images/5/6.png)

## Première expérimentation

J'ai alors décidé de passer à la phase de prototypage, étant satisfait de cette idée. Pour ce faire, la première étape fut de réaliser les cadres à imprimer en 3D. Pour cela j'ai utilisé le logiciel Fusion. L'assemblage se constitue de 4 cadres, 2 au milieu et 1 à chaque bout de l'assemblage. Ci-dessous, on peut voir les différents cadres, chaque cadre fait 14 cm x 14 cm de côté.

![](images/5/7.png)

On peut voir sur l'intérieur des cadres, prévu pour accueillir les LED que des petits crochés sont placés afin de maintenir les LED à l'intérieur du cadre. Aussi, sur le bas de chaque cadre, des pieds ont été modélisé pour permettre de poser les lampes sur un support et maintenir les différents éléments ensembles. Ci-dessous le support.

![](images/5/8.png)

Ensuite, j'ai réalisé le fichier vectoriel me permettant de découper les bandes de polypropylène (voir ci-dessous). On peut voir que 2 tailles de bandes sont nécessaires car ils faut que les bandes s’insèrent du côté extérieur et intérieur du cadre.

![](images/5/9.png)

Une fois tous les fichiers réalisés et exportés dans les bons formats je suis passé sur Prusa. (voir les paramètres ci-dessous)

![](images/5/10.png)
![](images/5/11.png)
![](images/5/12.png)

Une fois les Gcode exportés, je suis passé à l'impression 3D.

![](images/5/14.png)
![](images/5/15.png)

La phase suivante était le montage : pour les LED, j'ai opté pour une solution économique avec une guirlande en guise de LED achetée pour 2 euros dans un magasin Action. Pour ce modèle, j'ai eu besoin de 2 guirlandes. J'ai réalisé 2 ou 3 tours de fil dans les cadres, puis j’ai réalisé le passage du fil à travers les cadres dans une ouverture prévu à cette effet. (voir ci-dessous)

![](images/5/16.png)
![](images/5/17.png)

Pour la suite du montage, il est nécessaire d'insérer les bandes de polypropylène pliées préalablement comme ci-dessous.

![](images/5/18.png)

Ensuite, il est nécessaire d'assembler chaque module entre eux.

![](images/5/19.png)

Le résultat final

![](images/5/20.png)
![](images/5/21.png)
![](images/5/22.png)

## Useful links

- [.stl lampe avant](images/6/1.stl)
- [.stl lampe milieu](images/6/2.stl)
- [.stl lampe arrière](images/6/3.stl)
- [.dwg bandes](images/6/4.dwg)
- [.svg bandes](images/6/5.svg)

## Révision de la lampe

Suite à l'entrevue avec Victor, Gwen et Helene, plusieurs points-clés ont été soulevés sur ma lampe que j'ai essayé d'adresser par la suite. Il était notamment question de la complexité de la lampe et de son aspect massif. Pour répondre à ces questions, j'ai donc essayé de simplifier mon objet en revenant aux aspects essentiels de sa conception, ainsi par déconstruction de celui-ci j'ai réalisé des tests que l'on pourra observer par la suite. Une idée que Victor a évoqué m’a beaucoup parlé et j'ai donc décidé de m'intéresser à celle-ci durant la déconstruction de mon objet, l'idée étant de réaliser l'éclairage de ma lampe en projetant la lumière sur une surface. Ci-dessous on peut voir les différents tests que j'ai pu réaliser.

![](images/5/23.png)

Dans un premier temps, j'ai conservé le strict minimum de la lampe pour renforcer l'idée de cette forme indépendante qui prend fonction lorsqu'on l'allume.

![](images/5/24.png)

Dans un second temps, j'ai essayé de conserver les filtres de polypropylène pour voir ce que la lampe donnerai si elle était collée contre une paroi ou même éloignée de celle-ci mais avec un élément permettant de diriger la lumière. J'ai trouvé le résultat assez intéressant et j'ai donc eu l'idée de réaliser une lampe qui pourrait être accroché sur un mur ou posée sur un socle.

![](images/5/25.png)

Suite à ça, j'ai décidé de m'intéresser, dans un premier temps, à la question de la lampe sur un socle en me donnant la possibilité dans le futur d'adapter mon design pour le rendre versatile en fonction de son utilisation.

![](images/5/26.png)

Pour le pied, j'ai décidé de partir sur un pied rond pour faire référence à l'antenne de la télévision d'origine et à l'idée d'associer des formes opposées dans mon design.

![](images/5/27.png)

Pour ce prototype, j'ai décidé de m'éloigner du carré pour la forme du cadre car j'ai trouvé que la forme rectangulaire permettrait l'éclairage d'une surface plus importante et aussi de répondre à la question de la finesse de la lampe.

J'ai ensuite réalisé la modélisation sur Fusion du support et du cadre sur les mêmes principes que la première lampe, on peut voir ci-dessous une prévisualisation de celle-ci.

![](images/5/28.png)

A la suite de ça, je suis passé à l'impression, on peut voir ci-dessous les différentes pièces.

![](images/5/29.png)

J’ai ensuite assemblé ces pièces. Ci-dessous on peut observer la lampe lorsque qu'elle est éteinte ne laissant transparaitre que le cadre et son pied.

![](images/5/30.png)

Lorsque les guirlandes sont ajoutées, on peut apprécier le résultat.

![](images/5/31.png)

## Retour sur la lampe

Suite à une discussion entre Victor, Gwen, Helene et moi, il a été nécessaire d'envisager des révisions sur ma lampe. Dans un premier temps, le point crucial qui a été soulevé lors de notre entrevue et le manque de puissance lumineuse de la lampe qui ne permet pas d'apprécier les effets de lumière. Aussi, il était question du pied supportant le cadre et de mon rapport à l'objet de référence. Il m'a justement été fait remarquer que le pied ajouté à mon cadre m'éloignait du propos d'origine et de ce que je recherchais vis à vis de mon objet. En effet, le fait de rajouter cette anneau au bas du cadre donne une dénomination à mon objet et laisse transparaitre sa fonction d'origine.

Pour remédier à ces problèmes, j'ai décidé d'orienter ma recherche en fonction d'une source de lumière viable et non de la forme. Aussi j'ai voulu rester sur le même concept de bande de lumière contraint dans un cadre ou une forme indépendante. De cette façon, je me suis renseigné sur les questions de puissance lumineuse dans un premier temps. En me référant au tableau ci-dessous j'ai pu prendre note de quelle puissance était nécessaire pour produire un effet convenable vis à vis de ma lampe.

![](images/5/32.png)

Ainsi, il serait intéressant que ma lampe produise aux alentours de 700 lumens pour avoir une puissance convenable.

J'ai donc par la suite orienté ma recherche vers le type de source lumineuse qui était nécessaire pour réaliser mon projet sans trop dénaturer l'évolution qui avait était fait sur la question de la finesse de l'objet. Je me suis donc renseigné sur les rubans LED qui pouvaient tout à fait convenir à mon projet. Dans mon cas, le ruban LED a besoin de 3 caractéristiques principales:

* être assez fin pour pourvoir être inséré dans le cadre sans être vu
* être plié pour réaliser les angles
* être assez puissant

La première caractéristique fut assez facile à résoudre en sachant que les fil LED sont rarement plus larges que 1 cm, ainsi j'ai opté pour un ruban LED IP 20 car la résistance à l'eau de celui n'était pas nécessaire et cela permet de réduire les dimensions de celui-ci.

![](images/5/33.png)

Dimensions du ruban en centimètre: 500 x 1 x 0.2 cm

La deuxième caractéristique a été la plus compliqué à résoudre, je me suis donc renseigné sur le pliage d’un ruban LED mais cela ne me paraissait pas très viable car il était assez imprédictible de savoir si le ruban n’allait pas se casser un moment du pliage. J'ai donc opté pour des raccords trouvés en ligne permettant de réaliser exactement ce dont j'avais besoin. Les rubans étant coupable l'opération consistera à sectionner le ruban à certaines longueur en accord avec les dimensions du cadre et de les raccorder grâce au module ci-dessous.

![](images/5/34.png)

Enfin, pour la puissance, j'ai opté pour un ruban LED 12V avec des LED de 5mm par 5mm et une quantité de 60LED/m. Ce type de bandeau fournit 900 lumen/m, il me faudra donc à peu près 80 cm de ruban LED pour atteindre une puissance de 700 lumens. Ci-dessous les caractéristiques du ruban.

![](images/5/35.png)

Il m'a aussi été nécessaire de me doter d’un un adaptateur 220V - 12 V pour permettre le fonctionnement du ruban LED ainsi que des connecteurs en plus en cas de problèmes.

![](images/5/41.png)

Aussi, ce ruban étant dimmable, il sera possible d'acheter un dimmer pour régler la luminosité de la lampe si nécessaire.

Ainsi, je suis dans l'attente de ce matériel pour réaliser un prototype fonctionnel me permettant d'expérimenter si mon objet fonctionne.

## Pré-jury

FRAME

Mon objet est une lampe qui s'inspire de la Black 201 Télévision Set. Les 2 mots choisis pour l’objet sont inspiration et influence, car j'ai décidé de reprendre certains concepts clés de la télévision et d'en faire la base de mon objet. Le but est de réaliser une lampe qui à la façon de la télévision ne laisse pas transparaitre sa fonction d'origine lorsqu'elle n'est pas allumée. Pour ce faire, j'ai décidé de réaliser un cadre contenant un dispositif produisant de la lumière et la projetant sur une surface à l'arrière de celui-ci. Ainsi, l'objet repose sur lui-même et ne laisse à voir que sa forme lorsque éteint.

Ci-dessous, on peut voir des photos d'un prototype qui était à l'origine avec un pied que j'ai volontairement retiré suite au dernière échange.

![](images/5/36.png)
![](images/5/37.png)

La puissance lumineuse de la lampe n'étant pas encore satisfaisante, j'ai commencé à me renseigner pour la changer comme expliqué précédemment. Mais par souci de respect de l'exercice, je vais réaliser un tutoriel permettant la production de cette lampe même si le prochain modèle sera probablement très différent techniquement.

* **ETAPE 1**

Imprimer le cadre à l'imprimante 3D avec le fichier fournis ci-dessous

- [.stl cadre](images/6/4.stl)

Résultat:
![](images/5/38.png)

* **ETAPE 2**

Insérer la guirlande à l'arrière du cadre

Résultat:

![](images/5/39.png)

* **ETAPE 3**

Tourner autour du cadre en passant derrière les crochets jusqu'au bout de la guirlande.

Résultat:

![](images/5/40.png)

Pour la prochaine version, je réaliserai un tutoriel plus détaillé sur les différents branchements du bandeau LED et du matériel nécessaire.
